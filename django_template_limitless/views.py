class MenuGroup(object):
    def __init__(this, menu_name="whatever", menu_items=None, menu_icon="folder"):
        if menu_items is None:
            menu_items = list()

        this.menu_name = menu_name
        this.menu_items = menu_items
        this.menu_icon = menu_icon


class MenuLink(object):
    def __init__(this, link_name="whatever", link_url="/dashboard/"):
        this.link_name = link_name
        this.link_url = link_url
